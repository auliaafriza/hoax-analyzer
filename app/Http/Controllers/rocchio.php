<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Tdm;
use App\NaiveBaye;

class rocchio extends Controller
{
     public function index(){
    	return view('rocchio.index');
    }

    public function submit(){
    	$string  = $_POST['teks'];
    	$feature = $_POST['fitur'];
    	//echo "Hallo";
    	return $this->input($string, $feature);
    }


    public function input($string, $feature){
    	$kalimat = strtolower($string);
    	$kalimat = $this->removePunctuation($kalimat);
    	$kalimat = $this->normalization($kalimat);
    	if ($feature == "rocchio") {
    		$kalimat = $this->removeStopwords($kalimat);
    		$array_terms = $this->tokenization_unigram($kalimat);
    		$kelas = $this->classifyrocchio($array_terms);
    	}
    	else{
    		$kalimat = $this->removeStopwords($kalimat);
    		$array_terms = $this->tokenization($kalimat);
    		$kelas = $this->classifynaivebayes($array_terms);
    	}
    	echo "</br>";
    	//echo "<h6 style='margin-bottom:3px;'>execTime: ". round($lama, 3) ."sec</h6><h3><span>" . $kelas . "</span></h3>";

    }

    public function removePunctuation($string){
    	$kalimat = trim(preg_replace('/\s+/', ' ', $string));
    	$kalimat = preg_replace("/[^a-zA-Z ]+/", "", $kalimat);
    	return $kalimat;
    }

    public function normalization($string){
    	$pattern     = config('pattern');
    	$replacement = config('replacement');

    	$kalimat = preg_replace($pattern, $replacement, $string);
    	return $kalimat;
    }

    public function removeStopwords($string){
    	$stopwords 	= config('stopwords');

    	$kalimat = preg_replace($stopwords, "", $string);
    	return $kalimat;
    }

    public function tokenization_unigram($string){
		$array_text = explode(" ", $string);
		$dfs = \App\Df::where('feature_selection', true)->get()->toArray();
		$terms = array_column($dfs, 'term');
		$array_text = array_intersect($array_text, $terms);
		$array_text = array_count_values($array_text);

		return $array_text;
    }

    public function tokenization($string){
		return explode(" ", $string);
    }

    public function classifynaivebayes($var_array = array()){
    	
    	$freq_docHoax = Tdm::getTotalDocument(0,'HOAX');
	    $freq_docNonHoax = Tdm::getTotalDocument(0,'NONHOAX');
	    $total_doc = Tdm::getTotalDocument();
	    $peluang_Hoax = $freq_docHoax/$total_doc;
	    $peluang_NonHoax = $freq_docNonHoax/$total_doc;
	    $hasil = [];


		$naivebayes_hoax = 1;
		$naivebayes_nonhoax = 1;
		
		foreach ($var_array as $terms) {
			$nilai_naivebayes_hoax = DB::select('SELECT SUM(nilai_likelihood_hoax) as t FROM naive_bayes WHERE term = ?', [$terms]);
			$nilai_naivebayes_nonhoax = DB::select('SELECT SUM(nilai_likelihood_nonhoax) as f FROM naive_bayes WHERE term = ?', [$terms]);

			if ($nilai_naivebayes_hoax[0]->t!=null) {
					$naivebayes_hoax = $naivebayes_hoax*$nilai_naivebayes_hoax[0]->t; 
			}
			if($nilai_naivebayes_nonhoax[0]->f !=null){	
					$naivebayes_nonhoax = $naivebayes_nonhoax* $nilai_naivebayes_nonhoax[0]->f;
			}
		}

		$naivebayes_hoax = $naivebayes_hoax*$peluang_Hoax;
		$naivebayes_nonhoax = $naivebayes_nonhoax*$peluang_NonHoax;

		$hoax_value = $naivebayes_hoax/ ($naivebayes_hoax+$naivebayes_nonhoax);
		$nonhoax_value = $naivebayes_nonhoax/ ($naivebayes_hoax+$naivebayes_nonhoax);

		$hoax_value = $hoax_value*100;
		$nonhoax_value =$nonhoax_value*100;

		echo "Hoax Value     : " . number_format((float)$hoax_value, 2, '.', '') . "%";
		echo "<br/>";
		echo "Non Hoax Value : " . number_format((float)$nonhoax_value, 2, '.', '') . "%";
		echo "<br/>";
		echo "<br/>";
		echo "<div class='progress'>
		 		    <div class='progress-bar progress-bar-primary' role='progressbar' style='width: ".$hoax_value."%;height:20px'>
		 		    	".number_format((float)$hoax_value, 2, '.', '')."% HOAX
		 		    </div>
		 		    <div class='progress-bar progress-bar-info' role='progressbar' style='width:".$nonhoax_value."%;height:20px'>
		 		    	".number_format((float)$nonhoax_value, 2, '.', '')."% NONHOAX
				    </div>
			</div>";


    }

     public function classifyrocchio($var_array = array()){

    	$centroid_Hoax = 0;
		$centroid_NonHoax = 0;
		$nilai_p_vektor_hoax = 0;
		$nilai_p_vektor_nonhoax = 0;
		$nilai_sqrt_data_uji = 0;
		$nilai_t_uji = 0;

		foreach ($var_array as $terms => $frequency) {
			$nilai_centroid = DB::select('SELECT nilai_centroid_hoax as t, nilai_centroid_nonhoax as f FROM centroid_rocchios WHERE term = ?', [$terms]);
			$panjangvektor= DB::select('SELECT nilai_panjangvektor_hoax as k, nilai_panjangvektor_nonhoax as l FROM panjang_vektors');
			$nilai_df_uji = DB::select('SELECT df as df_uji FROM dfs WHERE feature_selection = 1 and term= ?',[$terms]);



			if ($nilai_centroid !=null) {
					$nilai_idf = 541/(($nilai_df_uji[0]->df_uji)+1);
					$nilai_idf_uji= log($nilai_idf,10);
					
					$nilai_tfidf_uji = $frequency * $nilai_idf_uji; 
					$nilai_t_uji += pow($nilai_tfidf_uji, 2);
					
					$centroid_Hoax += ($nilai_centroid[0]->t*$nilai_tfidf_uji);
					$centroid_NonHoax += ($nilai_centroid[0]->f*$nilai_tfidf_uji);
					
			}

		}

		if($nilai_t_uji == null){
			$nilai_hasil_centroid_hoax = 0;
			$nilai_hasil_centroid_nonhoax = 0;

			$nilai_presentasi_hoax = 0;
			$nilai_presentasi_nonhoax = 0;
		}
		else{
		$nilai_sqrt_data_uji = sqrt($nilai_t_uji);
		$nilai_p_vektor_hoax = ($nilai_sqrt_data_uji*$panjangvektor[0]->k);
		$nilai_p_vektor_nonhoax = ($nilai_sqrt_data_uji*$panjangvektor[0]->l);

		$nilai_hasil_centroid_hoax = $centroid_Hoax/($nilai_p_vektor_hoax);	
		$nilai_hasil_centroid_nonhoax = $centroid_NonHoax/($nilai_p_vektor_nonhoax);	

		$nilai_presentasi_hoax = $nilai_hasil_centroid_hoax/($nilai_hasil_centroid_hoax+$nilai_hasil_centroid_nonhoax);
		$nilai_presentasi_nonhoax = $nilai_hasil_centroid_nonhoax/($nilai_hasil_centroid_hoax+$nilai_hasil_centroid_nonhoax);
		}
		

		$nilai_presentasi_hoax = $nilai_presentasi_hoax*100;
		$nilai_presentasi_nonhoax = $nilai_presentasi_nonhoax*100;

		echo "Hoax Value     : " .$nilai_hasil_centroid_hoax. "<br/>";
		echo "Non Hoax Value : " .$nilai_hasil_centroid_nonhoax. "<br/>";
		echo "<br/>";
		echo "<div class='progress'>
		 		    <div class='progress-bar progress-bar-primary' role='progressbar' style='width: ".$nilai_presentasi_hoax."%;height:20px'>
		 		    	".number_format((float)$nilai_presentasi_hoax, 2, '.', '')."% HOAX
		 		    </div>
		 		    <div class='progress-bar progress-bar-info' role='progressbar' style='width:".$nilai_presentasi_nonhoax."%;height:20px'>
		 		    	".number_format((float)$nilai_presentasi_nonhoax, 2, '.', '')."% NONHOAX
				    </div>
			</div>";

    }
}
