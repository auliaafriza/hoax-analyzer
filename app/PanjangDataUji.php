<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PanjangDataUji extends BaseModel
{
    public static function generate(){
    	$dfs = Df::where('feature_selection', true)->get();
    	$N = Tdm::getTotalDocument();
    	//$nilai_simpan = [];
		foreach ($dfs as $df) {

			$tdms = Tdm::where('test_data', true)->where('term', $df->term)->get();
			
			foreach ($tdms as $tdm) {
				$idf = 0;
				if($tdm->frequency == 0){
					$nilai_N = $N+1;
					$nilai_df = $df->df;
					$idf = log($N/$nilai_df, 10);	
				}
				else{
					$nilai_N = $N+1;
					$nilai_df = $df->df+1;
					$idf = log($nilai_N/$nilai_df, 10);
				}

				$nilai_tfidf = $idf* $tdm->frequency;

				$DataUji = new PanjangDataUji();
				$DataUji->term = $tdm->term;
				$DataUji->nilai_total_tfidf = $nilai_tfidf;
				$DataUji->documents = $tdm->document;
				$DataUji->class = $tdm->class;
				$DataUji->save();

				//$array_push($nilai_simpan,$df->term, $N, $idf, $nilai_tfidf, $tdm->frequency,$nilai_df,$nilai_N);
			}

		}
		//$nilai_simpan_array = [
		//		'isi array push' => $nilai_simpan,
		//	];

		//return $nilai_simpan;

		return true;
 }

    public static function getUsingQuery()
	{
		return DB::select('SELECT * FROM panjang_data_ujis');
	}
}
