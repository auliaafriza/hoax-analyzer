<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NaiveBaye extends BaseModel
{
     public static function generatenb()
	{
		  $dfs = Df::where('feature_selection', true)->get();
		  $freq_kataHoax = Tdm::getTotalFrequency(0,'HOAX');
		  $freq_kataNonHoax = Tdm::getTotalFrequency(0,'NONHOAX');
		  $term_unik = Tdm::getTotalTermUnik();
		   foreach ($dfs as $df) 
		   {
				$jum_term_Hoax = Tdm::getTotalFrequency(0,'HOAX', $df->term);
			   	$jum_term_NonHoax = Tdm::getTotalFrequency(0,'NONHOAX', $df->term);

				$naivebayes = new NaiveBaye();
				$naivebayes->term = $df->term;
				$naivebayes->nilai_likelihood_hoax = ($jum_term_Hoax+1)/($freq_kataHoax+$term_unik);
				$naivebayes->nilai_likelihood_nonhoax = ($jum_term_NonHoax+1)/($freq_kataNonHoax+$term_unik);
				$naivebayes->save();
		    }
		
		return true;
	}

	public static function normalisasiLikelihood()
	{
			$hasil_likelihood = NaiveBaye::get();
			
			$max_hoax= NaiveBaye::max('nilai_likelihood_hoax');
			$max_nonhoax= NaiveBaye::max('nilai_likelihood_nonhoax');
			$min_hoax= NaiveBaye::min('nilai_likelihood_Hoax');
			$min_nonhoax= NaiveBaye::min('nilai_likelihood_NonHoax');

			if($max_hoax > $max_nonhoax){
				$max_range = $max_hoax;
			}
			else{
				$max_range = $max_nonhoax;
			}

			if($min_hoax > $min_nonhoax){
				$min_range = $min_hoax;
			}else{
				$min_range = $min_nonhoax;
			}

			$likelihood_Update = DB::statement("update naive_bayes set nilai_likelihood_hoax = (((nilai_likelihood_hoax - {$min_range})/({$max_range} - {$min_range}))*(0.9-0.8))+0.8, nilai_likelihood_nonhoax = (((nilai_likelihood_nonhoax - {$min_range})/({$max_range} - {$min_range}))*(0.9-0.8))+0.8");

			return true;
	}


	public static function classifynbargmax($id){
		$freq_dochoax = Tdm::getTotalDocument(0,'HOAX');
	    $freq_docnonhoax = Tdm::getTotalDocument(0,'NONHOAX');
	    $total_doc = Tdm::getTotalDocument();
	    $peluang_hoax = $freq_dochoax/$total_doc;
	    $peluang_nonhoax = $freq_docnonhoax/$total_doc;
	    $hasil = [];

	    $k_fold = KFold::find($id);
		$documents = explode(",", $k_fold->documents);

		foreach ($documents as $document) {
			
	    $naivebayes_hoax = log10($peluang_hoax);
		$naivebayes_nonhoax = log10($peluang_nonhoax);

			$tdms = Tdm::where('document', $document)->where('frequency','<>',0)->get();
			
			foreach ($tdms as $tdm) {
				$naivebayes = NaiveBaye::where('term',$tdm->term)->first();

				if ($naivebayes!=null) {
					
					$naivebayes_hoax += log10($naivebayes->nilai_likelihood_hoax)*($tdm->frequency); 
					$naivebayes_nonhoax += log10($naivebayes->nilai_likelihood_nonhoax)*($tdm->frequency);

					}
			
				}				
		
			if($naivebayes_hoax >= $naivebayes_nonhoax){
						$prediction = 'HOAX'; 
			}else{
						$prediction = 'NONHOAX';
			}

			$hasil[$document] = [
				'actual'=> $tdms[0]->class,
				'predict' => $prediction,
				'hoax' => $naivebayes_hoax,
				'nonhoax' => $naivebayes_nonhoax,
			];
			

		}

		return $hasil;
	} 


	public static function classifynb($id){
		$freq_dochoax = Tdm::getTotalDocument(0,'HOAX');
	    $freq_docnonhoax = Tdm::getTotalDocument(0,'NONHOAX');
	    $total_doc = Tdm::getTotalDocument();
	    $peluang_hoax = $freq_dochoax/$total_doc;
	    $peluang_nonhoax = $freq_docnonhoax/$total_doc;
	    $hasil = [];

	    $k_fold = KFold::find($id);
		$documents = explode(",", $k_fold->documents);

		foreach ($documents as $document) {
			
	    $naivebayes_hoax = ($peluang_hoax);
		$naivebayes_nonhoax = ($peluang_nonhoax);

			$tdms = Tdm::where('document', $document)->where('frequency','<>',0)->get();
			
			foreach ($tdms as $tdm) {
				$naivebayes = NaiveBaye::where('term',$tdm->term)->first();

				if ($naivebayes!=null) {
					
					$naivebayes_hoax *= pow($naivebayes->nilai_likelihood_hoax,$tdm->frequency); 
					$naivebayes_nonhoax *= pow($naivebayes->nilai_likelihood_nonhoax,$tdm->frequency);

					}
			
				}				
		
			if($naivebayes_hoax >= $naivebayes_nonhoax){
						$prediction = 'HOAX'; 
			}else{
						$prediction = 'NONHOAX';
			}

			$hasil[$document] = [
				'actual'=> $tdms[0]->class,
				'predict' => $prediction,
				'hoax' => $naivebayes_hoax,
				'nonhoax' => $naivebayes_nonhoax,
			];
			

		}

		return $hasil;
	} 


	public static function hitungAkurasi($id)
	{

		$hasil_klasifikasi = NaiveBaye::classifynb($id);

		$TP = 0;
		$TN = 0;
		$FP = 0;
		$FN = 0;
		$dokumen_benar = [];
		$benar = 0;
		$salah = 0;
		$total_dokumen_uji = count($hasil_klasifikasi);

		foreach ($hasil_klasifikasi as $id_dokumen => $hk) {
			if ($hk['actual'] == $hk['predict']) {
				if($hk['predict']=='HOAX'){
					$TP++;
				}
				else{
					$TN++;
				}
				$benar++;
				array_push($dokumen_benar, $id_dokumen);
			}
			else{
				if($hk['predict']=='HOAX'){
					$FP++;
				}
				
				else{
					$FN++;
				}
				$salah++;
			}
		}

		$hasil_akurasi = [
			'accuracy' => ($benar/$total_dokumen_uji)*100,
			'recall' => ($TP/($TP+$FN))*100,
			'precision' => ($TP/($TP+$FP))*100,
			'True Positif' => $TP,
			'True Negatif' => $TN,
			'False Positif' => $FP,
			'False Negatif' => $FN,
			'total dokumen benar' => $benar,
			'total dokumen salah' => $salah,
			'summary' => $dokumen_benar,
		];

		return $hasil_akurasi;

	}
}
