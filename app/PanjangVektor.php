<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PanjangVektor extends Model
{
 	 public static function generatepanjang()
	{
		  $dfs = Df::where('feature_selection', true)->get();
		  $nilai_t_centroid_hoax = 0; 
		  $nilai_t_centroid_nonhoax = 0;

		   foreach ($dfs as $df) 
		   {
				
			   $rocchio = CentroidRocchio::where('term',$df->term)->first();

			   if ($rocchio!= null) {
			   $nilai_t_centroid_hoax += pow($rocchio->nilai_centroid_hoax, 2);
			   $nilai_t_centroid_nonhoax += pow($rocchio->nilai_centroid_nonhoax, 2);			   	
			   }				
		    }
		    $nilai_sqrt_centroid_hoax = sqrt($nilai_t_centroid_hoax);
			$nilai_sqrt_centroid_nonhoax = sqrt($nilai_t_centroid_nonhoax);
			
			    $panjang_vektor = new PanjangVektor();
				$panjang_vektor->nilai_panjangvektor_hoax = $nilai_sqrt_centroid_hoax;
				$panjang_vektor->nilai_panjangvektor_nonhoax = $nilai_sqrt_centroid_nonhoax;
				$panjang_vektor->save();

		return true;
	}
}
