<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CentroidRocchio extends BaseModel
{
    public static function generatecentroid()
	{
		  $dfs = Df::where('feature_selection', true)->get();
		  $n_hoax = Tdm::getTotalDocument(0,'HOAX');
		  $n_nonhoax = Tdm::getTotalDocument(0,'NONHOAX');
		  //$nilai_simpan = [];
		  $nilai_hoax = 0; 
		  $nilai_nonhoax = 0;

		   foreach ($dfs as $df) 
		   {
				$jum_tfidf_hoax = TfIdf::getTotalTFIDF('HOAX', $df->term);
			   	$jum_tfidf_nonhoax = TfIdf::getTotalTFIDF('NONHOAX', $df->term);

			   	$nilai_hoax = $jum_tfidf_hoax/$n_hoax;
				$nilai_nonhoax = $jum_tfidf_nonhoax/$n_nonhoax;			   	

				$centroid_rocchio_t = new CentroidRocchio();
				$centroid_rocchio_t->term = $df->term;
				$centroid_rocchio_t->nilai_centroid_hoax = $nilai_hoax;
				$centroid_rocchio_t->nilai_centroid_nonhoax = $nilai_nonhoax;
				$centroid_rocchio_t->save();
		}
		return true;
}

	public static function classify($id){
		$k_fold = KFold::find($id);
		$documents = explode(",", $k_fold->documents);
		$hasil = [];

		foreach ($documents as $document) {
			
			//mengambil dokumen uji dari tdm berdasarkan id dokumen pada tabel k fold
			//$tdms = Tdm::where('document', $document)->where('frequency','<>',0)->get();
			$dataujis = PanjangDataUji::where('documents', $document)->where('nilai_total_tfidf','<>',0)->get();

			$centroid_Hoax = 0;
			$centroid_NonHoax = 0;
			$nilai_t_uji = 0;
			$nilai_p_vektor_hoax = 0;
			$nilai_p_vektor_nonhoax = 0;
			$nilai_sqrt_data_uji = 0;
			$panjangvektor = DB::select('SELECT nilai_panjangvektor_hoax as t, nilai_panjangvektor_nonhoax as f FROM panjang_vektors');
			
				foreach ($dataujis as $datauji) {
				$rocchio = CentroidRocchio::where('term',$datauji->term)->first();
				

				if ($rocchio!=null) {
					
					$nilai_t_uji += pow($datauji->nilai_total_tfidf, 2);	
					$centroid_Hoax += ($rocchio->nilai_centroid_hoax*$datauji->nilai_total_tfidf);
					$centroid_NonHoax += ($rocchio->nilai_centroid_nonhoax*$datauji->nilai_total_tfidf);
					}
			
				}			

			$nilai_sqrt_data_uji = sqrt($nilai_t_uji);
			$nilai_p_vektor_hoax = ($nilai_sqrt_data_uji*$panjangvektor[0]->t);
			$nilai_p_vektor_nonhoax = ($nilai_sqrt_data_uji*$panjangvektor[0]->f);
			
			$nilai_hasil_centroid_hoax = $centroid_Hoax/($nilai_p_vektor_hoax);
			$nilai_hasil_centroid_nonhoax = $centroid_NonHoax/($nilai_p_vektor_nonhoax);	

			if($nilai_hasil_centroid_hoax >= $nilai_hasil_centroid_nonhoax){
						$prediction = 'HOAX'; 
			}else{
						$prediction = 'NONHOAX';
			}

			$hasil[$document] = [
				'actual'=> $dataujis[0]->class,
				'predict' => $prediction,
				'hoax' => $nilai_hasil_centroid_hoax,
				'nonhoax' => $nilai_hasil_centroid_nonhoax,
			];
			

		}

		return $hasil;
	} 

	public static function hitungAkurasi($id)
	{

		$hasil_klasifikasi = CentroidRocchio::classify($id);

		$benar = 0;
		$salah = 0;
		$TP = 0;
		$TN = 0;
		$FP = 0;
		$FN = 0;
		$dokumen_benar = [];
		$dokumen_salah = [];
		$total_dokumen_uji = count($hasil_klasifikasi);

		foreach ($hasil_klasifikasi as $id_dokumen => $hk) {
			if ($hk['actual'] == $hk['predict']) {
				if($hk['predict']=='HOAX'){
					$TP++;
				}
				else{
					$TN++;
				}
				$benar++;
				array_push($dokumen_benar, $id_dokumen);
			}
			else{
				if($hk['predict']=='HOAX'){
					$FP++;
				}
				
				else{
					$FN++;
				}
				$salah++;
				array_push($dokumen_salah, $id_dokumen);
			}
		}

		$hasil_akurasi = [
			'accuracy' => ($benar/$total_dokumen_uji)*100,
			'recall' => ($TP/($TP+$FN))*100,
			'precision' => ($TP/($TP+$FP))*100,
			'True Positif' => $TP,
			'True Negatif' => $TN,
			'False Positif' => $FP,
			'False Negatif' => $FN,
			'total dokumen benar' => $benar,
			'total dokumen salah' => $salah,
			'summary' => $dokumen_benar,
			'dokumen salah' => $dokumen_salah,
		];

		return $hasil_akurasi;

	}


}
