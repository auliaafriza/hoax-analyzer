<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroidRocchiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centroid_rocchios', function (Blueprint $table) {
            $table->string('term');
            $table->double('nilai_centroid_hoax');
            $table->double('nilai_centroid_nonhoax');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centroid_rocchios');
    }
}
