<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('author/', 'AuthorController@index');
Route::get('chart/{session_id}', 'ChartController@index');

Route::get('book/', 'BookController@index');
Route::get('author/create/', 'AuthorController@create');
Route::get('author/edit/{id}', 'AuthorController@edit');
Route::get('author/delete/{id}', 'AuthorController@delete');

Route::post('author/create/', 'AuthorController@store');
Route::post('author/edit/{id}', 'AuthorController@update');

Route::get('/', 'rocchio@index');
Route::post('rocchio/submit', 'rocchio@submit');